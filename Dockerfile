# minimal linux distribution with official node  image
FROM node:13-alpine 

# Install the 'serve' npm package
RUN npm install -g serve

# Copy 'public' to 'public'
COPY public public

# When the container start, serve the public/ dir
ENTRYPOINT [ "serve", "-n", "public/" ]